package com.rikkei.tranning.activity;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mp3;
    int timeCurrent = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mp3 = MediaPlayer.create(MainActivity.this, R.raw.test);
        mp3.start();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mp3.pause();
        timeCurrent = mp3.getCurrentPosition();
        outState.putInt("time", timeCurrent);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        timeCurrent = savedInstanceState.getInt("time");
        //Log.d("TimeR", timeCurrent + "");
        mp3.seekTo(timeCurrent);
        mp3.start();

    }
}
